package de.gorg.api.data;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Membership {
	
	@Id private long id;
	private long userId;
	private long learngroupId;
	
	public Membership() {
		super();
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getLearngroupId() {
		return learngroupId;
	}

	public void setLearngroupId(long learngroupId) {
		this.learngroupId = learngroupId;
	}

	@Override
	public String toString() {
		return "Membership [id=" + id + ", userId=" + userId + ", learngroupId=" + learngroupId + "]";
	}
	
}

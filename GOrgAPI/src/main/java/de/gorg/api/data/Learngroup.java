package de.gorg.api.data;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Learngroup {

	@Id private long id;
	private String name;
	private String icon;
	private List<Membership> memberships;
	
	public Learngroup() {
		super();
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public List<Membership> getMemberships() {
		if (memberships == null) {
			memberships = new ArrayList<Membership>();
		}
		return memberships;
	}

	@Override
	public String toString() {
		return "Learngroup [id=" + id + ", name=" + name + ", icon=" + icon + ", memberships=" + memberships + "]";
	}
	
}

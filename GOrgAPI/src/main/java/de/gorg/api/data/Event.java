package de.gorg.api.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Event {
	
	@Id private long id;
	private Date startDate;
	private Date endDate;
	private Location location;
	private String topic;
	private long learngroupId;
	private List<Participation> participations;
	
	public Event() {
		super();
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getStartDate() {
		return startDate;
	}
	
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	public Date getEndDate() {
		return endDate;
	}
	
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	public Location getLocation() {
		return location;
	}
	
	public void setLocation(Location location) {
		this.location = location;
	}
	
	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public long getLearngroupId() {
		return learngroupId;
	}

	public void setLearngroupId(long learngroupId) {
		this.learngroupId = learngroupId;
	}
	
	public List<Participation> getParticipations() {
		if(participations == null) {
			participations = new ArrayList<Participation>();
		}
		return participations;
	}

	@Override
	public String toString() {
		return "Event [id=" + id + ", startDate=" + startDate + ", endDate=" + endDate + ", location=" + location
				+ ", topic=" + topic + ", learngroupId=" + learngroupId + ", participations=" + participations + "]";
	}
	
	
}

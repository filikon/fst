package de.gorg.api.data;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Participation {
	
	@Id private long id;
	private long userId;
	private long eventId;
	
	public Participation() {
		super();
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public long getUserId() {
		return userId;
	}
	
	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	public long getEventId() {
		return eventId;
	}
	
	public void setEventId(long eventId) {
		this.eventId = eventId;
	}

	@Override
	public String toString() {
		return "Participation [id=" + id + ", userId=" + userId + ", eventId=" + eventId + "]";
	}
	
}

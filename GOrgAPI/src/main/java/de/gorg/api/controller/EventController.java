package de.gorg.api.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.gorg.api.data.Event;
import de.gorg.api.data.Participation;
import de.gorg.api.repo.EventRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/event")
@CrossOrigin
@Api
public class EventController {

	@Autowired private EventRepository eventRepo;
	
	@GetMapping(value = "/{eventId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation("Lädt einen bestehenden Termin")
	public Event getEvent(@PathVariable long eventId) {
		Optional<Event> event = eventRepo.findById(eventId);
		return event.isPresent() ? event.get() : null;
	}
	
	@GetMapping(value = "/learngroup/{learngroupId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation("Lädt alle Events einer Lerngruppe")
	public List<Event> getLearngroupEvents(@PathVariable long learngroupId) {
		return eventRepo.findByLearngroupId(learngroupId);
	}
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation("Erstellt einen neuen Termin")
	public boolean newEvent(@RequestBody Event event) {
		if(event.getId() == 0L) {
			event.setId(nextEventId());
		}
		eventRepo.insert(event);
		return true;
	}
	
	@PutMapping(value = "/{eventId}", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation("Bearbeitet einen bestehenden Termin")
	public boolean edit(@PathVariable long eventId, @RequestBody Event event) {
		Optional<Event> optional = eventRepo.findById(eventId);
		if(optional.isPresent()) {
			Event editEvent = optional.get();
			editEvent.setStartDate(event.getStartDate());
			editEvent.setEndDate(event.getEndDate());
			editEvent.setLocation(event.getLocation());
			editEvent.setTopic(event.getTopic());
			eventRepo.save(editEvent);
			return true;
		}
		return false;
	}
	
	@DeleteMapping(value = "/{eventId}")
	@ApiOperation("Löscht einen bestehenden Termin")
	public boolean delete(@PathVariable long eventId) {
		try {
			eventRepo.deleteById(eventId);
			return true;
		} catch (IllegalArgumentException e) {
			return false;
		}
	}
	
	@PostMapping(value = "/{eventId}/participate")
	@ApiOperation("Nimmt an einem Termin teil")
	public boolean participate(@PathVariable long eventId) {
		Optional<Event> optional = eventRepo.findById(eventId);
		if(optional.isPresent()) {
			Event event = optional.get();
			Participation participation = new Participation();
			participation.setId(event.getParticipations().size()+1);
			participation.setEventId(eventId);
			participation.setUserId(0);
			event.getParticipations().add(participation);
			eventRepo.save(event);
			return true;
		}
		return false;
	}
	
	private long nextEventId() {
		Optional<Event> event = eventRepo.findTopByOrderByIdDesc();
		return event.isPresent() ? event.get().getId() + 1 : 1L;
	}

}

package de.gorg.api.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.gorg.api.data.Event;
import de.gorg.api.data.User;
import de.gorg.api.repo.UserRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/user")
@CrossOrigin
@Api
public class UserController {
	
	@Autowired private UserRepository userRepo;
	
	@GetMapping(value = "/{userId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation("Lädt einen bestehenden Benutzer")
	public User getUser(@PathVariable long userId) {
		Optional<User> user = userRepo.findById(userId);
		return user.isPresent() ? user.get() : null;
	}
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation("Erstellt einen neuen Benutzer")
	public boolean newUser(@RequestBody User user) {
		if(user.getId() == 0L) {
			user.setId(nextId());
		}
		userRepo.insert(user);
		return true;
	}
	
	private long nextId() {
		Optional<User> user = userRepo.findTopByOrderByIdDesc();
		return user.isPresent() ? user.get().getId() + 1 : 1L;
	}

}

package de.gorg.api.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.gorg.api.data.Learngroup;
import de.gorg.api.data.Membership;
import de.gorg.api.repo.LearngroupRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/learngroup")
@CrossOrigin
@Api
public class LearngroupController {
	
	@Autowired private LearngroupRepository learngroupRepo;
	
	@GetMapping(value = "/{learngroupId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation("Lädt einen bestehenden Lerngruppe")
	public Learngroup getLearngroup(@PathVariable long learngroupId) {
		Optional<Learngroup> learngroup = learngroupRepo.findById(learngroupId);
		return learngroup.isPresent() ? learngroup.get() : null;
	}
	
	@GetMapping(value = "/user/{userId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation("Lädt alle Lerngruppen eines Users")
	public List<Learngroup> getUserLearngroups(@PathVariable long userId) {
		return learngroupRepo.findByMemberships_UserId(userId);
	}
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation("Erstellt eine neue Lerngruppe")
	public boolean newLearngroup(@RequestBody Learngroup learngroup) {
		if(learngroup.getId() == 0L) {
			learngroup.setId(nextLearngroupId());
		}
		learngroupRepo.insert(learngroup);
		return true;
	}
	
	@PutMapping(value = "/{learngroupId}", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation("Bearbeitet einen bestehenden Lerngruppe")
	public boolean edit(@PathVariable long learngroupId, @RequestBody Learngroup learngroup) {
		Optional<Learngroup> optional = learngroupRepo.findById(learngroupId);
		if(optional.isPresent()) {
			Learngroup editLearngroup = optional.get();
			editLearngroup.setName(learngroup.getName());
			editLearngroup.setIcon(learngroup.getIcon());
			learngroupRepo.save(editLearngroup);
			return true;
		}
		return false;
	}
	
	@DeleteMapping(value = "/{learngroupId}")
	@ApiOperation("Löscht einen bestehenden Lerngruppe")
	public boolean delete(@PathVariable long learngroupId) {
		try {
			learngroupRepo.deleteById(learngroupId);
			return true;
		} catch (IllegalArgumentException e) {
			return false;
		}
	}
	
	@PostMapping(value = "/{learngroupId}/join")
	@ApiOperation("Tritt einer Lerngruppe bei")
	public boolean join(@PathVariable long learngroupId) {
		Optional<Learngroup> optional = learngroupRepo.findById(learngroupId);
		if(optional.isPresent()) {
			Learngroup learngroup = optional.get();
			Membership membership = new Membership();
			membership.setId(learngroup.getMemberships().size()+1);
			membership.setLearngroupId(learngroupId);
			membership.setUserId(0);
			learngroup.getMemberships().add(membership);
			learngroupRepo.save(learngroup);
			return true;
		}
		return false;
	}
	
	private long nextLearngroupId() {
		Optional<Learngroup> learngroup = learngroupRepo.findTopByOrderByIdDesc();
		return learngroup.isPresent() ? learngroup.get().getId() + 1 : 1L;
	}

}

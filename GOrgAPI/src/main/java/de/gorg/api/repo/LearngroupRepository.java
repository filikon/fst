package de.gorg.api.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import de.gorg.api.data.Learngroup;

public interface LearngroupRepository extends MongoRepository<Learngroup, Long>{
	
	public List<Learngroup> findByMemberships_UserId(long userId);
	
	public Optional<Learngroup> findTopByOrderByIdDesc();

}

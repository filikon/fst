package de.gorg.api.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import de.gorg.api.data.Event;

public interface EventRepository extends MongoRepository<Event, Long>{
	
	public List<Event> findByLearngroupId(long learngroupId);
	
	public Optional<Event> findTopByOrderByIdDesc();

}

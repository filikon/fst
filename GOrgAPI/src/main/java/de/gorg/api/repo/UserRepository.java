package de.gorg.api.repo;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import de.gorg.api.data.User;

public interface UserRepository extends MongoRepository<User, Long>{
	
	public Optional<User> findTopByOrderByIdDesc();

}

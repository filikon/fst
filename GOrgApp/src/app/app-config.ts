import {Injectable} from '@angular/core';
import {IAppConfig} from './shared/models/app-config.model';
import {environment} from '../environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class AppConfig {

    public static config: IAppConfig;

    constructor(private http: HttpClient) {

    }

    public load(): Promise<void> {
        const jsonFile = `assets/config/config.${environment.name}.json`;
        const prom = new Promise<void>((resolve, reject) => {
            this.http.get(jsonFile).toPromise().then((response: IAppConfig) => {
                AppConfig.config = <IAppConfig>response;
                resolve();
            }).catch((response: any) => {
                reject(`Could not load file '${jsonFile}': ${JSON.stringify(response)}`);
            });
        });
        return prom;
    }
}

import {Injectable} from '@angular/core';
import {AppConfig} from '../app-config';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs/index';
import {ILearngroup} from '../shared/models/leangroup.model';

@Injectable({
    providedIn: 'root'
})
export class LearngroupService {

    constructor(private http: HttpClient) {
    }

    public getMyGroups(): Observable<ILearngroup[]> {
        return this.http.get<ILearngroup[]>(AppConfig.config.apiEndpoint + '/learngroup/user/1');
    }

    public getGroup(id: string): Observable<ILearngroup> {
        return this.http.get<ILearngroup>(AppConfig.config.apiEndpoint + '/learngroup/' + id);
    }
}

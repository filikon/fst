import {Injectable} from '@angular/core';
import {AppConfig} from '../app-config';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/index';
import {IUser} from '../shared/models/user.model';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    constructor(private http: HttpClient) {
    }

    public getUser(id: string): Observable<IUser> {
        return this.http.get<IUser>(AppConfig.config.apiEndpoint + '/user/' + id);
    }
}

import {Injectable} from '@angular/core';
import {AppConfig} from '../app-config';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/index';
import {IEvent} from '../shared/models/event.model';

@Injectable({
    providedIn: 'root'
})
export class EventService {

    constructor(private http: HttpClient) {
    }

    public getEvent(id: string): Observable<IEvent> {
        return this.http.get<IEvent>(AppConfig.config.apiEndpoint + '/event/' + id);
    }

    public deleteEvent(id: string): Observable<IEvent> {
        return this.http.post<boolean>(AppConfig.config.apiEndpoint + '/event/' + id + '/delete');
    }

    public getEventsForLearngorup(learngroupId: string): Observable<IEvent[]> {
        return this.http.get<IEvent[]>(AppConfig.config.apiEndpoint + '/event/learngroup/' + learngroupId);
    }

    public createEvent(event: IEvent): Observable<boolean> {
        return this.http.post<boolean>(AppConfig.config.apiEndpoint + '/event', event);
    }
}

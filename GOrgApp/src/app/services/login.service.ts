import {Injectable} from '@angular/core';
import {AppConfig} from '../app-config';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs/index';

@Injectable({
    providedIn: 'root'
})
export class LoginService {

    constructor(private http: HttpClient) {
    }

    public login(user: string, password: string): Observable<boolean> {
        return of(true);
        //return this.http.get<boolean>(AppConfig.config.apiEndpoint + '/login');
    }
}

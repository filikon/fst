import {Component} from '@angular/core';
import {LoginService} from '../../services/login.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent {
    public userName: string;
    public password: string;

    constructor(private loginService: LoginService, private router: Router) {
    }

    public login(): void {
        this.loginService.login(this.userName, this.password).subscribe((loginSuccess: boolean) => {
            if (loginSuccess) {
                this.router.navigate(['dashboard']);
            }
        });
    }
}

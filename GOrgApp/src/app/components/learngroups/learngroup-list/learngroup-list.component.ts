import {Component, OnInit} from '@angular/core';
import {ILearngroup} from '../../../shared/models/leangroup.model';
import {LearngroupService} from '../../../services/learngroup.service';
import {Router} from "@angular/router";

@Component({
    selector: 'app-learngroup-list',
    templateUrl: './learngroup-list.component.html',
    styleUrls: ['./learngroup-list.component.css']
})
export class LearngroupListComponent implements OnInit {

    public learngroups: ILearngroup[];

    constructor(private learngroupService: LearngroupService, private router: Router) {

    }

    ngOnInit() {
        this.learngroupService.getMyGroups().subscribe((groups) => this.learngroups = groups);
    }

}

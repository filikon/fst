import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {ILearngroup} from '../../../shared/models/leangroup.model';
import {LearngroupService} from '../../../services/learngroup.service';
import {switchMap} from 'rxjs/operators';
import {IEvent} from '../../../shared/models/event.model';
import {UserService} from '../../../services/user.service';
import {IUser} from '../../../shared/models/user.model';
import {EventService} from '../../../services/event.service';

@Component({
    selector: 'app-learngroup-detail',
    templateUrl: './learngroup-detail.component.html',
    styleUrls: ['./learngroup-detail.component.css']
})
export class LearngroupDetailComponent implements OnInit {

    public learngroup: ILearngroup;
    private events: IEvent[];
    private displayedMembershipColumns: string[];
    private displayedEventColumns: string[];
    public membershipsLoaded: boolean;
    public eventsLoaded: boolean;

    constructor(private route: ActivatedRoute, private learngroupService: LearngroupService, private userService: UserService,
                private eventService: EventService, private changeDetectorRefs: ChangeDetectorRef) {
        this.membershipsLoaded = false;
        this.eventsLoaded = false;
        this.learngroup = {
            memberships: []
        };
        this.events = [];
        this.displayedMembershipColumns = ['userName', 'firstName', 'lastName', 'membershipStartDate'];
        this.displayedEventColumns = ['title', 'startDate', 'city', 'actions'];
    }

    ngOnInit() {
        this.route.paramMap.pipe(
            switchMap((params: ParamMap) => {
                return this.learngroupService.getGroup(params.get('id'));
            })
        ).subscribe((learngroup: ILearngroup) => {
            let toLoad = learngroup.memberships.length;
            for (let x = 0; x < learngroup.memberships.length; x++) {
                this.userService.getUser(learngroup.memberships[x].userId).subscribe((user: IUser) => {
                    learngroup.memberships[x].user = user;
                    this.checkCompletion(--toLoad);
                });
            }
            this.checkCompletion(toLoad);
            learngroup.events = [];
            this.eventService.getEventsForLearngorup(learngroup.id).subscribe((events: IEvent[]) => {
                events.map((currentEvent) => {
                    learngroup.events.push(currentEvent);
                    currentEvent.startDate = new Date(currentEvent.startDate);
                });
                this.eventsLoaded = true;
            });
            this.learngroup = learngroup;
            this.events = learngroup.events;
        });
    }

    public deleteEvent(id: string) {
        this.eventService.deleteEvent(id).subscribe(result => {
            if (result) {
                this.events = this.events.filter(e => e.id != id);
            }
        });
        this.events = this.events.filter(e => e.id != id);
    }

    private checkCompletion(toLoad: number) {
        if (toLoad === 0) {
            this.membershipsLoaded = true;
        }
    }

}

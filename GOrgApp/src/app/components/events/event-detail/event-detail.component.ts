import {Component, OnInit} from '@angular/core';
import {IEvent} from '../../../shared/models/event.model';
import {switchMap} from 'rxjs/internal/operators';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {ILearngroup} from '../../../shared/models/leangroup.model';
import {EventService} from '../../../services/event.service';
import * as moment from 'moment';
import {Observable, of} from 'rxjs/index';
import {Location} from '@angular/common';

@Component({
    selector: 'app-event-detail',
    templateUrl: './event-detail.component.html',
    styleUrls: ['./event-detail.component.css']
})
export class EventDetailComponent implements OnInit {

    public event: IEvent;

    constructor(private route: ActivatedRoute, private eventService: EventService, private location: Location) {
    }

    private setTime(time: string) {
        const date = moment(time, 'hh:mm');
        this.event.startDate.setHours(date.hours(), date.minutes());
    }

    private save() {
        if (!this.event.id) {
            this.event.startDate = this.event.startDate.toISOString() as any;
            this.eventService.createEvent(this.event).subscribe(result => {
                if (result) {
                    this.location.back();
                }
            });
        }
    }

    private delete() {
        if (this.event.id) {
            this.eventService.deleteEvent(this.event.id).subscribe(result => {
                if (result) {
                    this.location.back();
                }
            });
        }
    }

    ngOnInit() {
        this.route.paramMap.pipe(
            switchMap((params: ParamMap) => {
                const id = params.get('id');
                const learngroupId = params.get('learnGroupId');
                if (id) {
                    return this.eventService.getEvent(params.get('id'));
                } else {
                    const cities = ['Dortmund', 'Gummersbach'];
                    return of({
                        learngroupId: learngroupId,
                        location: {
                            city: cities[this.getRandomInt(cities.length)]
                        }
                    } as IEvent);
                }
            })
        ).subscribe((event: IEvent) => {
            event.startDate = new Date(event.startDate)
            this.event = event;
        });
    }

    private getRandomInt(max) {
        return Math.floor(Math.random() * Math.floor(max));
    }

}

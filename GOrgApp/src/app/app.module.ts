import {BrowserModule} from '@angular/platform-browser';
import {APP_INITIALIZER, NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {HttpClientModule} from '@angular/common/http';

import {AppComponent} from './app.component';
import {LoginComponent} from './components/login/login.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppConfig} from './app-config';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {PageNotFoundComponent} from './components/page-not-found/page-not-found.component';
import {LearngroupListComponent} from './components/learngroups/learngroup-list/learngroup-list.component';
import {LearngroupDetailComponent} from './components/learngroups/learngroup-detail/learngroup-detail.component';
import {MatIconModule} from '@angular/material/icon';
import {MatListModule, MatNativeDateModule} from '@angular/material';
import {MatTableModule} from '@angular/material/table';
import {EventDetailComponent} from './components/events/event-detail/event-detail.component';

export function initializeApp(appConfig: AppConfig) {
    return () => appConfig.load();
}

const appRoutes: Routes = [
    {path: 'dashboard', component: DashboardComponent},
    {path: 'learngroups', component: LearngroupListComponent},
    {path: 'learngroup/:id', component: LearngroupDetailComponent},
    {path: 'event/create/:learnGroupId', component: EventDetailComponent},
    {path: 'event/:id', component: EventDetailComponent},
    {path: '', component: LoginComponent},
    {path: '**', component: PageNotFoundComponent}
];

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        DashboardComponent,
        PageNotFoundComponent,
        LearngroupListComponent,
        LearngroupDetailComponent,
        EventDetailComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        HttpClientModule,
        MatInputModule,
        MatButtonModule,
        MatListModule,
        MatTableModule,
        MatIconModule,
        MatDatepickerModule,
        MatNativeDateModule,
        RouterModule.forRoot(
            appRoutes,
            {enableTracing: true} // <-- debugging purposes only
        )
    ],
    providers: [
        AppConfig, {
            provide: APP_INITIALIZER,
            useFactory: initializeApp,
            deps: [AppConfig],
            multi: true
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}

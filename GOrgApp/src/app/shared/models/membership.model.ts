import {ILearngroup} from './leangroup.model';
import {IUser} from './user.model';

export interface IMembership {
    startDate?: Date;
    endDate?: Date;
    learngroup?: ILearngroup;
    user?: IUser;
    userId?: string;
}

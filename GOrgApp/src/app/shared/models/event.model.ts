import {ILocation} from './location.model';
export interface IEvent {
    id: string;
    topic: string;
    startDate: Date;
    endDate?: Date;
    location: ILocation;
    learngroupId?: string;
}

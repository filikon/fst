export interface IUser {
    userName: string;
    firstName: string;
    lastName: string;
    password?: string;
}

import {IMembership} from './membership.model';
import {IEvent} from './event.model';

export interface ILearngroup {
    id?: string;
    name?: string;
    icon?: string;
    memberships?: IMembership[];
    events?: IEvent[];
}
